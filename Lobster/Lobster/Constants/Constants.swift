//
//  Constants.swift
//  Lobster
//
//  Created by JustDoIt on 07.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

// MARK: - kConstraints
let kAccessToken = "access_token"

// MARK: - General
let appModel = AppModel.sharedInstance
let appDelegate = UIApplication.shared.delegate as? AppDelegate
let window = appDelegate?.window


// MARK: - Storyboards
let loginStoryboard = UIStoryboard.init(name: "Login", bundle: Bundle.main)
let restaurantsCollectionStoryboard = UIStoryboard.init(name: "RestaurantCollection", bundle: Bundle.main)
let restaurantStoryboard = UIStoryboard.init(name: "Restaurant", bundle: Bundle.main)


// MARK: - Features
func localize(_ string: String!) -> String {
    return LocalizeHelper.shared.localizedString(forKey: string)
}
