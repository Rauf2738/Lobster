//
//  API.swift
//  Roomster
//
//  Created by Igor Tudoran on 27.03.2018.
//  Copyright © 2018 Roomster. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

protocol RequestProtocol {
    var baseURL: String { get }
    var path: Path { get }
    var fullURL: String { get }
    var encoding: ParameterEncoding { get }
    var method: HTTPMethod { get }
    var headers: HTTPHeaders { get }
    var parameters: Parameters { get }
    var queryID: String? { get }
}

let serverURL = "http://5.45.124.84:8080/lobster/"

typealias APIResponse = ((JSON, LBError?) -> Void)

@objc class API: NSObject {
    @objc static let shared = API()
    var manager: SessionManager!
    
    override private init() {
        super.init()
        let headers = Alamofire.SessionManager.defaultHTTPHeaders
        
        /*var userAgent = "Roomster/"
        if let version = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            userAgent.append(version)
        }
        let device = UIDevice.current
        userAgent.append(" (\(device.model); \(device.systemName) \(device.systemVersion); ")
        userAgent.append("Scale/\(UIScreen.main.scale))")
        headers["User-Agent"] = userAgent*/
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = headers
        manager = SessionManager(configuration: config)
    }
}

extension API {
    func request(_ request: APIRequest, completion: @escaping APIResponse) {
        let apiRequest = manager.request(request.fullURL,
                                      method: request.method,
                                      parameters: request.parameters,
                                      encoding: request.encoding,
                                      headers: request.headers)
            .validate()
            .responseData { response in
                let statusCode = response.response?.statusCode ?? -1000
                print("Status Code: ", statusCode)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSON: ", json)
                    completion(json, nil)
                case .failure(let error):
                    let err = LBError(message: error.localizedDescription,
                                      code: statusCode,
                                      data: response.data)
                    if let data = response.data,
                        let errorString = String(data: data, encoding: String.Encoding.utf8) {
                        print(errorString)
                    }
                    print("Error: ", err)
                    completion(JSON.null, err)
                }
        }
        print(apiRequest.description)
    }
}

extension API {
    func login(email: String, password: String, completion: @escaping (Bool)->()) {
        let params = ["email": email, "password": password]
        let request = APIRequest.login(body: params)
        manager.request(request.fullURL,
                        method: request.method,
                        parameters: request.parameters,
                        encoding: request.encoding,
                        headers: request.headers)
            .validate()
            .responseData { response in
                let statusCode = response.response?.statusCode ?? -1000
                print("Status Code: ", statusCode)
                switch response.result {
                case .success(_):
                    if let token = response.response?.allHeaderFields["token"] {
                        UserDefaults.standard.set(token, forKey: kAccessToken)
                        UserDefaults.standard.synchronize()
                        completion(true)
                    } else {
                        completion(false)
                    }
                case .failure(_):
                    completion(false)
                }
        }
    }
}

enum Path: String {
    case createAccount = "users/register"
    case login = "login"
    case me = "users/me"
    case getRestaurants = "restaurants"
}

enum APIRequest {
    case me()
    case createAccount(body: Parameters)
    case login(body: Parameters)
}

extension RequestProtocol {
    
    var baseURL: String {
        return serverURL
    }
    
    var fullURL: String {
        let url = baseURL + path.rawValue
        return url
    }
    
}

extension APIRequest: RequestProtocol {
    
    var path: Path {
        switch self {
        case .createAccount:
            return .createAccount
        case .me:
            return .me
        case .login:
            return .login
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .createAccount,
             .login:
            return .post
        case .me:
            return .get
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .createAccount,
             .login:
            return JSONEncoding.default
        default:
            return URLEncoding.default
        }
    }
    
    var parameters: Parameters {
        var parameters: Parameters
        switch self {
        case .createAccount(let body),
             .login(let body):
            parameters = body
        default:
            parameters = Parameters()
        }
        print("parameters: \(parameters)")
        return parameters
    }
    
    var queryID: String? {
        return nil
    }
    
    var headers: HTTPHeaders {
        let headers = HTTPHeaders()
        return headers
    }
}


