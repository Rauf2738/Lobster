//
//   ContinueRegisterManageActions.swift
//  Lobster
//
//  Created by JustDoIt on 23.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

extension ContinueRegisterViewController {
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        if (validateRegisterFields()) {
            let phoneNumber = phoneNumberTextField.text?.components(separatedBy: .whitespaces).joined()
            let params = ["fullName" : fullNameTextField.text, "phone" : phoneNumber, "email" : email, "password" : password]
            API.shared.request(.createAccount(body: params)) { (response, error) in
                let alertController = UIAlertController(title: "Success!", message: nil, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (_) in
                    self.view.endEditing(true)
                }))
                self.present(alertController, animated: true, completion: nil)
//                window?.rootViewController = appModel.tabBarViewController
            }
        }
    }
    
}
