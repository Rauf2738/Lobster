//
//   ContinueRegisterViewController.swift
//  Lobster
//
//  Created by JustDoIt on 20.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit
import PhoneNumberKit

class  ContinueRegisterViewController: UIViewController {
    
    var email: String!
    var password: String!
    
    private var imagePicker: UIImagePickerController!
    
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: PhoneNumberTextField!
    
    @IBOutlet weak var whiteViewTopConstraint: NSLayoutConstraint!
    
    override func loadView() {
        super.loadView()
        
        // Avatar Image View
        self.avatarImageView.isUserInteractionEnabled = true
        let avatarImageViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(avatarImageViewPressed))
        self.avatarImageView.addGestureRecognizer(avatarImageViewTapGesture)
        
        // Keyboard Hide When Tapping Empty View
        let viewTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(viewTap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func avatarImageViewPressed() {
        imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.whiteViewTopConstraint.constant = 50
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.whiteViewTopConstraint.constant = 180
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
}

extension ContinueRegisterViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            avatarImageView.image = image
        }
        self.avatarImageView.layer.masksToBounds = true
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.height/2
        self.dismiss(animated: true, completion: nil)
    }
    
}
