//
//  LoginViewControllerStates.swift
//  Lobster
//
//  Created by JustDoIt on 09.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

extension LoginAndRegisterViewController {
    
    func changeState(state: LoginViewStates) {
        switch state {
        case .signUp:
            activateSignUpView()
        case .signIn:
            activateSignInView()
        }
        currentState = state
    }
    
    private func activateSignUpView() {
        UIView.transition(with: whiteView, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            self.centerLabel.text = localize("SIGN UP")
            self.changeStateButton.setTitle(localize("Have account? Sign In"), for: .normal)
            self.loginButton.setTitle(localize("Sign Up"), for: .normal)
            self.forgetPasswordButton.isHidden = false
        }, completion: nil)
    }
    
    private func activateSignInView() {
        UIView.transition(with: whiteView, duration: 0.5, options: .transitionFlipFromRight, animations: {
            self.centerLabel.text = localize("SIGN IN")
            self.changeStateButton.setTitle(localize("Don't have an account? Sign Up"), for: .normal)
            self.loginButton.setTitle(localize("Sign In"), for: .normal)
            self.forgetPasswordButton.isHidden = true
        }, completion: nil)
    }
    
}
