//
//  RegisterViewControllerActions.swift
//  Lobster
//
//  Created by JustDoIt on 16.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit


extension LoginAndRegisterViewController {
    
    @IBAction func changeStateButtonPressed(_ sender: Any) {
        switch currentState {
        case .signUp?:
            changeState(state: .signIn)
        case .signIn?:
            changeState(state: .signUp)
        default:
            changeState(state: .signUp)
        }
        resetValidationWarnings()
    }
    
    @IBAction func signInOrSignUpPressed(_ sender: Any) {
        if (currentState == LoginViewStates.signIn) {
            login(email: emailTextField.text!, password: passwordTextField.text!)
        } else if (currentState == LoginViewStates.signUp) {
            if (validateRegisterFields() == true) {
                appModel.continueRegisterViewController.modalTransitionStyle = .crossDissolve
                appModel.continueRegisterViewController.email = self.emailTextField.text
                appModel.continueRegisterViewController.password = self.passwordTextField.text
                present(appModel.continueRegisterViewController, animated: true, completion: nil)
            }
        }
    }
    
    func login(email: String, password: String) {
//        "qwerty@test.com"
//        "qwerty1234"
        API.shared.login(email: email, password: password) { response in
            if response == true {
                window?.rootViewController = appModel.tabBarViewController
            } else {
                let alertController = UIAlertController.init(title: "Wrong!", message: "Wrong email or password", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (alertAction) in
                    self.view.endEditing(true)
                }))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func resetValidationWarnings() {
        emailTextField.layer.borderWidth = 0.0
        passwordTextField.layer.borderWidth = 0.0
    }
    
}
