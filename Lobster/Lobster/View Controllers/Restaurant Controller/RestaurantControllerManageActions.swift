//
//  RestaurantControllerManageActions.swift
//  Lobster
//
//  Created by JustDoIt on 18.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

extension RestaurantController {
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
