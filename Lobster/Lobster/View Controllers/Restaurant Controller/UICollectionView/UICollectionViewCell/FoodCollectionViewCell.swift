//
//  FoodCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 11.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class FoodCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var generalImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    func configure(with food: Food!) {
        
    }
}
