//
//  RestaurantController.swift
//  Lobster
//
//  Created by JustDoIt on 09.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class RestaurantController: UIViewController {
    
    @IBOutlet weak var menuCollectionView: UICollectionView!
    
    override func loadView() {
        super.loadView()
    }
    
}

extension RestaurantController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let foodCell = collectionView.dequeueReusableCell(withReuseIdentifier: "foodCell", for: indexPath) as! FoodCollectionViewCell
        foodCell.configure(with: nil)
        return foodCell
    }
    
}
