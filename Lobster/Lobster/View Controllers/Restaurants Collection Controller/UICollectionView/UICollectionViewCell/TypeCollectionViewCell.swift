//
//  TypeCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 02.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class TypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backgrndView: UIView!
    
    func configure(title: String) {
        titleLabel.text = title
    }
    
    func setState(isVisible: Bool) {
        if isVisible == true {
            backgrndView.backgroundColor = UIColor(red:0.95, green:0.73, blue:0.23, alpha:1.0)
            titleLabel.tintColor = UIColor(red:0.95, green:0.73, blue:0.23, alpha:1.0)
        } else {
            backgrndView.backgroundColor = .white
            titleLabel.tintColor = .black
        }
    }
    
}
