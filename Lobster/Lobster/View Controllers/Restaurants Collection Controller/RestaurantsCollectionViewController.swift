//
//  RestaurantsCollectionViewController.swift
//  Lobster
//
//  Created by JustDoIt on 25.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class RestaurantsCollectionViewController : UIViewController {
    
    @IBOutlet weak var typeCollectionView: UICollectionView!
    @IBOutlet weak var restaurantsCollectionView: UICollectionView!
    
    let kTypeCell = "TypeCell"
    let kRestaurantCell = "RestaurantCell"
    
    var currentRestarauntType: String!
    var restaurantTypes: [String] = [String]()
    var currentRestarauntTypeIndexPath: IndexPath!
    
    
    override func loadView() {
        super.loadView()
        restaurantTypes.append("Azerbaijan")
        restaurantTypes.append("Ukraine")
        restaurantTypes.append("Russia")
        restaurantTypes.append("Kazahstan")
        restaurantTypes.append("China")
        restaurantTypes.append("Japanesse")
    }
    
}

extension RestaurantsCollectionViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return restaurantTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.restaurantsCollectionView {
            let restaurant = Restaraunt(id: 1, name: "Svecha")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kRestaurantCell, for: indexPath) as! RestaurantCollectionViewCell
            cell.configure(restaurant: restaurant)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kTypeCell, for: indexPath) as! TypeCollectionViewCell
            cell.configure(title: restaurantTypes[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == self.restaurantsCollectionView {
            
        } else {
            if (indexPath == currentRestarauntTypeIndexPath) {
                (cell as! TypeCollectionViewCell).setState(isVisible: true)
            } else {
                (cell as! TypeCollectionViewCell).setState(isVisible: false)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.restaurantsCollectionView {
            openRestaurant(nil)
        } else {
            setRestarauntType(indexPath: indexPath)
        }
    }
    
}
