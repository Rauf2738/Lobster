//
//  ScannerProtocol.swift
//  Lobster
//
//  Created by JustDoIt on 04.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ScannerDelegate {
    func scanner(getResposne response: JSON)
}
