//
//  UIFont+Extension.swift
//  Lobster
//
//  Created by JustDoIt on 07.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

extension UIFont {
    func standardLabelFont(size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Regular", size: size)!
    }
}
