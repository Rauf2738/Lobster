//
//  Restaurant.swift
//  Lobster
//
//  Created by JustDoIt on 25.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

class Restaraunt {
    let id: Int
    let name: String!
    var type: String!
    var imageUrl: URL!
    var reviewsCount: Int!
    var rating: Double!
    var averageCheck: Double!
    var description: String!
    var coordinateLat: Double!
    var coordinateLng: Double!
    var phones: Array<String>!
    var socialConnects: Array<String>!
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}
