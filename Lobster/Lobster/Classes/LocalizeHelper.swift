//
//  LocalizeHelper.swift
//  Lobster
//
//  Created by JustDoIt on 16.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation


class LocalizeHelper {
    static let shared = LocalizeHelper()

    func localizedString(forKey key: String) -> String {
        return Bundle.main.localizedString(forKey: key, value: "", table: nil)
    }
    
}
