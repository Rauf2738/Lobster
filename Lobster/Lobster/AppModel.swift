//
//  AppModel.swift
//  Lobster
//
//  Created by Rauf on 24.09.17.
//  Copyright © 2017 RGS. All rights reserved.
//

import UIKit
import GoogleSignIn

class AppModel: NSObject {

    static let sharedInstance = AppModel()
    
    // States
    var isAuthorized: Bool!
    
    // ViewControllers
    var loginAndRegisterViewController: LoginAndRegisterViewController!
    var continueRegisterViewController: ContinueRegisterViewController!
    var restarauntsCollectionViewController: RestaurantsCollectionViewController!
    var restaurantViewController: RestaurantController!
    var tabBarViewController: UITabBarController!
    
    private override init() {
        super.init()
        GIDSignIn.sharedInstance().clientID = "41588123346-4t2ltdkv3vp81jbot6rk7rdb8i2ehepm.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        isAuthorized = checkIsAuthorized()
        loginAndRegisterViewController = loginStoryboard.instantiateViewController(withIdentifier: "LoginAndRegisterViewController") as! LoginAndRegisterViewController
        continueRegisterViewController = loginStoryboard.instantiateViewController(withIdentifier: "ContinueRegisterViewController") as! ContinueRegisterViewController
        restarauntsCollectionViewController = restaurantsCollectionStoryboard.instantiateViewController(withIdentifier: "RestaurantsCollectionViewController") as! RestaurantsCollectionViewController
        restaurantViewController = restaurantStoryboard.instantiateViewController(withIdentifier: "RestaurantController") as! RestaurantController
        
        let scannerViewContoller = ScannerViewController()
        scannerViewContoller.tabBarItem = UITabBarItem.init(title: "", image: UIImage.init(named: "QRCode_button"), tag: 0)
        let viewcontrollers = [restarauntsCollectionViewController]
        tabBarViewController = UITabBarController()
        tabBarViewController.viewControllers = viewcontrollers as? [UIViewController]
    }
    
    private func checkIsAuthorized() -> Bool {
        if UserDefaults.standard.value(forKey: kAccessToken) != nil {
            return true
        } else {
            return false
        }
    }
    
}

extension AppModel: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard error != nil, user.serverAuthCode != nil else {
            print(error.localizedDescription)
            return
        }
        print(user.profile.name)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
}
